# DetectAgeGender_Web
Detect age and gender using caffe models

## Usage
1. First run `detect_age_gender.py`, then a webservice is published in your computer: http://localhost:5000
2. Change images path and `uri_base` in `demo.py`, then run `demo.py`

## Some Key techniques within this project
- Upload files with flask
    - [How to use flask](https://zhuanlan.zhihu.com/p/23731819?refer=flask)
    - [Flask api document](http://flask.pocoo.org/docs/0.11/api/#incoming-request-data)
- Detect age and gender using caffe
    - [Free source for detect age and gender](https://www.openu.ac.il/home/hassner/projects/cnn_agegender/)
- Upload files using python without 3rd part libs.
    - [Request with python](http://www.matteomattei.com/http-https-get-and-post-requests-with-python-including-file-upload/)

