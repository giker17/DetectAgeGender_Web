import os
import re
import json
import caffe
import threading
import traceback
from datetime import datetime as dt
from flask import Flask, request

from utils import write_log_daily_and_email as logging

age_list = [(0, 2), (4, 6), (8, 12), (15, 20), (25, 32), (38, 43), (48, 53), (60, 100)]
gender_list = ['male', 'female']
classifier_age, classifier_gender = None, None
logger = None

app = Flask(__name__)


# ディレクトリ削除
def remove_dir(path):
    if os.path.exists(path):
        if os.path.isfile(path):
            os.remove(path)
        elif os.path.isdir(path):
            for dpath in os.listdir(path):
                remove_dir(os.path.join(path, dpath))
            os.rmdir(path)


# ファイル種類、ファイルサイズチェック
def check_image_file():
    for f in request.files:
        file = request.files[f]
        filename = file.filename
        if re.match(r'.*\.(jpg|png|jpeg|bmp)$', filename.strip().lower()) is None:
            return 1
        file.seek(0, os.SEEK_END)
        size = file.tell()
        file.seek(0,os.SEEK_SET)
        if size > app.config['MAX_FILE_SIZE']:
            return 2
    return 0


# ファイルを一時的に保存し、ファイルオブジェクトを取得する
def get_image_file():
    img_dir = os.path.join(app.config['IMAGE_PATH'], dt.now().strftime('%Y%m%d%H%M%S%f'))
    if not os.path.exists(img_dir):
        os.makedirs(img_dir)

    input_images = []
    for f in request.files:
        file = request.files[f]
        image_full_path = os.path.join(img_dir, file.filename)
        file.save(image_full_path)

        input_image = caffe.io.load_image(image_full_path)
        input_images.append(input_image)
    remove_dir(img_dir)
    return input_images


# クライアントIPと処理スレッドIDを取得する
def get_ip_thread():
    thread_id = str(threading.get_ident())
    access_route = getattr(request, 'access_route')
    ips = ''
    for ip in access_route:
        ips += '|' + str(ip)
    if len(ips) > 0:
        ips = ips[1:]
    return 'IP: %s ThreadId: %s' % (ips, thread_id)


# 年齢・性別を推定する
@app.route('/DetectAgeGender', methods=['POST'])
def detect_age_gender():
    ip_thread = get_ip_thread()
    logger.info('%s [処理開始] ' % ip_thread)

    result = {}
    file_num = len(request.files)
    if file_num > 0:
        try:
            result_check = check_image_file()
            if result_check == 0:
                input_images = get_image_file()
                age_prediction = classifier_age.predict(input_images)
                gender_prediction = classifier_gender.predict(input_images)
                detection = []
                for i in range(file_num):
                    age_from, age_to = age_list[age_prediction[i].argmax()]
                    gender = gender_list[gender_prediction[i].argmax()]
                    detection.append({'AgeFrom': age_from, 'AgeTo': age_to, 'Gender': gender})
                result = {'IsSuccess': 0, 'Data': detection}
                logger.info('%s [年齢計算結果] %s' % (ip_thread, str(age_prediction)))
                logger.info('%s [性別計算結果] %s' % (ip_thread, str(gender_prediction)))
            elif result_check==1:
                result = {'IsSuccess': 1, 'Data': [], 'ErrorCode': '1002', 'ErrorMessage': '処理できないファイル種類です。'}
            elif result_check == 2:
                result = {'IsSuccess': 1, 'Data': [], 'ErrorCode': '1003', 'ErrorMessage': 'ファイルサイズは4MB超えています。'}
        except Exception as e:
            result = {'IsSuccess': 1, 'Data': [], 'ErrorCode': '1099', 'ErrorMessage': '致命エラーがありました。'}
            logger.error('%s [処理失敗] ' % ip_thread, exc_info=True)
    else:
        result = {'IsSuccess': 1, 'Data': [], 'ErrorCode': '1001', 'ErrorMessage': 'ファイルを指定してください。'}

    logger.info('%s [処理結果] %s' % (ip_thread, result))
    logger.info('%s [処理終了]' % ip_thread)
    return json.dumps(result)


# 初期化
def init_app_logger():
    global logger
    app.config['IMAGE_PATH'] = os.path.join(os.getcwd(), r'image_file')
    app.config['MODEL_PATH'] = os.path.join(os.getcwd(), r'cnn_age_gender_models_and_data')
    app.config['LOG_PATH'] = os.path.join(os.getcwd(), 'log')
    app.config['LOG_FILENAME'] = 'detect_age_gender.log'
    app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
    app.config['MAX_FILE_SIZE'] = 4 * 1024 * 1024

    path_log = app.config['LOG_PATH']
    if not os.path.exists(path_log):
        os.makedirs(path_log)
    logger = logging.create_timed_rotating_log(os.path.join(app.config['LOG_PATH'], app.config['LOG_FILENAME']),
                                               loggername='detect_age_gender')


# データモデルをロードする
def load_model():
    global classifier_age, classifier_gender

    logger.info('[データモデルロード開始]')
    mean_filename = os.path.join(app.config['MODEL_PATH'], r'mean.binaryproto')
    with open(mean_filename, "rb") as file:
        proto_data = file.read()
    a = caffe.io.caffe_pb2.BlobProto.FromString(proto_data)
    mean = caffe.io.blobproto_to_array(a)[0]

    age_net_pretrained = os.path.join(app.config['MODEL_PATH'], r'age_net.caffemodel')
    age_net_model_file = os.path.join(app.config['MODEL_PATH'], r'deploy_age.prototxt')
    classifier_age = caffe.Classifier(age_net_model_file, age_net_pretrained,
                                      mean=mean,
                                      channel_swap=(2, 1, 0),
                                      raw_scale=255,
                                      image_dims=(256, 256))

    gender_net_pretrained = os.path.join(app.config['MODEL_PATH'], r'gender_net.caffemodel')
    gender_net_model_file = os.path.join(app.config['MODEL_PATH'], r'deploy_gender.prototxt')
    classifier_gender = caffe.Classifier(gender_net_model_file, gender_net_pretrained,
                                         mean=mean,
                                         channel_swap=(2, 1, 0),
                                         raw_scale=255,
                                         image_dims=(256, 256))
    logger.info('[データモデルロード終了]')


if __name__ == "__main__":
    init_app_logger()
    load_model()
    app.run(threaded=True)
