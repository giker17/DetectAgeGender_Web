import time


def main():
    import requests

    uri_base = 'http://172.17.10.36:81/flask/DetectAgeGender'
    image_file0 = r'D:\download\example_image.jpg'
    image_file1 = r'D:\download\2017092613234264.jpg'
    image_file2 = r'D:\download\lena_1.jpg'
    image_file3 = r'D:\download\fe8585d6fadf6875bec35ca52d7ef.jpg'
    image_file4 = r'D:\download\fe8585d6fadf6875bec35ca52d7ef.jpg'

    try:
        files = {
            'file': open(image_file0, "rb"),
            # 'file1': open( image_file1, "rb" ),
            'file2': open(image_file2, "rb"),
            'file3': open(image_file3, "rb"),
            'file4': open(image_file3, "rb"),
        }
        begin = int(time.time() * 1000 + 0.5)
        response = requests.request(
            'POST',
            uri_base,
            params = None,
            files = files,
            verify = False)
        end = int(time.time() * 1000 + 0.5)
        t = end - begin

        print(t, response.json())
    except Exception as e:
        print('Error:')
        print(e)


def main1():
    from utils import request

    uri_base = 'http://172.17.10.36:81/flask/DetectAgeGender'
    image_file0 = r'D:\download\example_image.jpg'
    image_file1 = r'D:\download\2017092613234264.jpg'
    image_file2 = r'D:\download\lena_1.jpg'
    image_file3 = r'D:\download\fe8585d6fadf6875bec35ca52d7ef.jpg'
    image_file4 = r'D:\download\fe8585d6fadf6875bec35ca52d7ef.jpg'

    try:
        files = {
            'file': image_file0,
            # 'file1': image_file1,
            'file2': image_file2,
            'file3': image_file3,
            'file4': image_file4,
        }
        begin = int(time.time() * 1000 + 0.5)
        res = request.request(uri_base, files = files)
        end = int(time.time() * 1000 + 0.5)
        t = end - begin

        print(t, res)
    except Exception as e:
        print('Error:')
        print(e)


if __name__ == '__main__':
    main()
