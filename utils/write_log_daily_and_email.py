import logging
from logging.handlers import TimedRotatingFileHandler
from logging import handlers
import socket
 
def create_timed_rotating_log(path, loggername = None):
    """create day rotating loggger object.
    
    creat day rotating logger object and set the maximum count 
    of log files to 30. 

    Args:
        path: log file path.
    Returns:
        logger: logger object.    
    """
    if loggername is None:
        loggername = ''

    logger = logging.getLogger(loggername)
    logger.setLevel(logging.INFO)
 
    handler = TimedRotatingFileHandler(path,
                                       when="midnight", # midnight
                                       interval=1,
                                       backupCount=30)

    logFormatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    handler.setFormatter(logFormatter)
    logger.addHandler(handler)

    return logger


def create_critical_email_log():
    """create critical log object send email automaticly.
    """

    # email setup 
    HOST = 'email0Host'
    FROM = 'email0'
    TO = 'email1,email2'
    CREDENTIALS = ('email0','passwd')

    SUBJECT = 'New Critical Event From [Product Heatmap]'

    # Setup logging
    logging.basicConfig(level=logging.INFO)
    handler = handlers.SMTPHandler(HOST, FROM, TO, SUBJECT,CREDENTIALS)
    email_logger = logging.getLogger('Critical email log')
    email_logger.addHandler(handler)
    email_logger.setLevel = logging.CRITICAL

    return email_logger

 
if __name__ == "__main__":

    # creat log object
    log_file = "product_heatmap.log"
    my_logger = create_timed_rotating_log(log_file)

    # wirte infomation to log file
    for i in range(3):
        my_logger.info('this is a test')

    # wirte error to log file    
    try:
        open('/path/to/does/not/exist', 'rb')
    except Exception as e:
        my_logger.error('Failed to open file', exc_info=True)      

    # create email log object
    email_logger = create_critical_email_log()

    # send critical log with_email
    try:
        email_logger.critical('Critical Event Notification121212121212~~~~~~')
    except socket.error as error:
        my_logger.critical('Could not send email via SMTPHandler: %r', error)



