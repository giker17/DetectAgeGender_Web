#!/usr/bin/env python3
# This script performs a POST request through HTTP/HTTPS using
# builtin python3 moudules. There is also a class to encode files for upload!
# source: http://www.matteomattei.com/http-https-get-and-post-requests-with-python-including-file-upload/

import codecs
import http.client
import io
import json
import mimetypes
import os
import re
import sys
import uuid

USERAGENT = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'


class MultipartFormdataEncoder(object):
    def __init__(self):
        self.boundary = uuid.uuid4().hex
        self.content_type = 'multipart/form-data; boundary={}'.format(self.boundary)

    @classmethod
    def u(cls, s):
        if sys.hexversion < 0x03000000 and isinstance(s, str):
            s = s.decode('utf-8')
        if sys.hexversion >= 0x03000000 and isinstance(s, bytes):
            s = s.decode('utf-8')
        return s

    def iter(self, fields, files):
        """
        fields is a sequence of (name, value) elements for regular form fields.
        files is a sequence of (name, filename, file-type) elements for data to be uploaded as files
        Yield body's chunk as bytes
        """
        encoder = codecs.getencoder('utf-8')
        for (key, value) in fields:
            key = self.u(key)
            yield encoder('--{}\r\n'.format(self.boundary))
            yield encoder(self.u('Content-Disposition: form-data; name="{}"\r\n').format(key))
            yield encoder('\r\n')
            if isinstance(value, int) or isinstance(value, float):
                value = str(value)
            yield encoder(self.u(value))
            yield encoder('\r\n')
        for (key, filename, fpath) in files:
            key = self.u(key)
            filename = self.u(filename)
            yield encoder('--{}\r\n'.format(self.boundary))
            yield encoder(self.u('Content-Disposition: form-data; name="{}"; filename="{}"\r\n').format(key, filename))
            yield encoder(
                'Content-Type: {}\r\n'.format(mimetypes.guess_type(filename)[0] or 'application/octet-stream'))
            yield encoder('\r\n')
            with open(fpath, 'rb') as fd:
                buff = fd.read()
                yield (buff, len(buff))
            yield encoder('\r\n')
        yield encoder('--{}--\r\n'.format(self.boundary))

    def encode(self, fields, files):
        body = io.BytesIO()
        for chunk, chunk_len in self.iter(fields, files):
            body.write(chunk)
        return self.content_type, body.getvalue()


def parse_url(url):
    m = re.match(r'(https?)://(\d+.\d+.\d+.\d+|[^/^:]*):?(\d+)?(/.*)', url)

    remote_protocol, remote_host, remote_port, remote_api = m.groups()
    if remote_port is None:
        remote_port = 80
    return remote_protocol, remote_host, remote_port, remote_api


def request(url, fields = (), files = ()):
    remote_protocol, remote_host, remote_port, remote_api = parse_url(url)
    _fields = []
    for key in fields:
        _fields.append((key, fields[key]))
    _files = []
    for key in files:
        _files.append((key, os.path.basename(files[key]), files[key]))

    content_type, body = MultipartFormdataEncoder().encode(_fields, _files)

    h = None
    if remote_protocol == 'http':
        h = http.client.HTTPConnection(remote_host, remote_port)
    else:
        h = http.client.HTTPSConnection(remote_host, remote_port)
    headers = {
        'User-Agent': USERAGENT,
        'Content-Type': content_type
    }
    h.request('POST', remote_api, body, headers)
    res = h.getresponse()
    return json.loads(res.read().decode('utf-8'))


def main():
    url = 'http://172.17.10.36:81/flask/DetectAgeGender'
    image_file0 = r'D:\download\example_image.jpg'
    image_file1 = r'D:\download\lena_1.jpg'

    files = {
        'file0': image_file0,
        'file1': image_file1,
    }
    obj = request(url = url, files = files)
    print(obj)


if __name__ == '__main__':
    main()
